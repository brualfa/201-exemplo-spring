package tech.mastertech.itau.produto.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.repositories.CategoriaRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CategoriaService.class)
public class CategoriaServiceTest {
  @Autowired
  private CategoriaService sujeito;
  
  @MockBean
  private CategoriaRepository categoriaRepository;
  
  @Test
  public void deveSalvarUmaCategoria() {
    //given
    Categoria categoria = new Categoria();
    categoria.setNome("Alcool");

    when(categoriaRepository.save(categoria)).thenReturn(categoria);    
    
    //when
    Categoria categoriaSalva = sujeito.setCategoria(categoria);
    
    //expect
    assertEquals(categoriaSalva.getNome(), categoria.getNome());
  }
  
  @Test
  public void deveRetornarListaDeCategorias() {
    Categoria categoria = new Categoria();
    categoria.setId(1);
    categoria.setNome("Alcool");
    
    List<Categoria> categorias = Lists.list(categoria);
    
    when(categoriaRepository.findAll()).thenReturn(categorias);
    
    Iterable<Categoria> categoriasEncontradasIterable = sujeito.getCategorias();
    List<Categoria> categoriasEncontradas = Lists.newArrayList(categoriasEncontradasIterable);
    
    assertEquals(1, categoriasEncontradas.size());
    assertEquals(categoria, categoriasEncontradas.get(0));
  }
  
  @Test(expected = Exception.class)//trabalhar com excessão específica
  public void deveLancarExcessaoQuandoSalvarCategoriaSemNome() {
    Categoria categoria = new Categoria();
    
    when(categoriaRepository.save(categoria)).thenThrow(new Exception(""));
    
    sujeito.setCategoria(categoria);
  }
}
